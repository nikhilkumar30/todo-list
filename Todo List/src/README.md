# App Component Explanation

The `App` component is the root component of the website. It serves as the entry point for rendering the entire application.

## Components Used

- **Provider**: Provided by `react-redux`, this component allows the Redux store to be accessible to all components in the application.
- **ChakraProvider**: Provided by `@chakra-ui/react`, this component is the context provider for Chakra UI styles and components.

## Structure

The `App` component consists of the following elements:

1. **Provider and ChakraProvider**: These components wrap the entire application, providing access to the Redux store and Chakra UI styles.
2. **Flex Container**: This is a flex container that positions its children in a row layout.
   - Background color: `#8758ff`.
   - Minimum height: `100vh` to ensure the container covers the entire viewport.
   - Justify content: `center` to horizontally center its children.
   - Align items: `center` to vertically center its children.
3. **Box Component**: This is a container for the main content of the website.
   - Background color: `#1A1A40`.
   - Padding: `4` on all sides.
   - Border radius: `md` for slightly rounded corners.
   - Responsive width: Adjusted using `maxW` property to occupy different percentages of the viewport width based on screen size (`base`, `md`, `lg`). Horizontally centered using `mx="auto"`.
   - Responsive padding: Adjusted using `p` property to provide spacing around the content based on screen size.
4. **Center Component**: This centers its children horizontally and vertically within the Box component.
5. **VStack Component**: This vertically stacks its children.
   - Spacing: `8` to provide consistent vertical spacing between elements.
   - Alignment: `center` to center its children horizontally.
6. **Heading Component**: Renders the main heading of the website (`To Do List`).
   - Size: `xl` for extra-large.
   - Color: `white`.
7. **TaskInput Component**: Renders the input field for adding tasks.
8. **TaskList Component**: Renders the list of tasks.

## Functionality

- The `App` component provides the basic structure of the website, including layout and styling.
- It ensures that the Redux store is available to all components via the `Provider`.
- It ensures consistent styling and design using Chakra UI components and styles.

# TaskInput Component Explanation

The `TaskInput` component is responsible for rendering an input field and a button to add new tasks to the to-do list.

## Components Used

- **useState**: Imported from `react`, this hook is used to manage state within a functional component.
- **useDispatch**: Imported from `react-redux`, this hook is used to dispatch actions to the Redux store.
- **Input**: Imported from `@chakra-ui/react`, this component renders an input field for user input.
- **Button**: Imported from `@chakra-ui/react`, this component renders a button for triggering the task addition.
- **Flex**: Imported from `@chakra-ui/react`, this component provides a flexible container for aligning its children.

## Functionality

- **State Management**: The component uses the `useState` hook to manage the state of the input field (`task`) where the user enters the task text.
- **Event Handlers**: 
  - `handleChange`: This function is called whenever the input field value changes. It updates the `task` state with the new value.
  - `handleSubmit`: This function is called when the user clicks the "Add Task" button. It dispatches an `addTask` action with the new task details (ID and text) to the Redux store, clears the input field, and resets the `task` state.
- **Styling**: 
  - The input field has a placeholder text "What is the task today?" and is styled with a white text color and a background color of `#8758ff`.
  - The button has a background color of `#8758ff`.
  - The `Flex` container is used to align the input field and button horizontally for medium-sized screens (`md: "row"`), and vertically for smaller screens (`base: "column"`).
# TaskList Component Explanation

The `TaskList` component renders a list of tasks, allowing users to interact with each task by marking it as completed, editing its text, or deleting it.

## Components Used

- **useState**: Imported from `react`, this hook is used to manage state within a functional component.
- **useEffect**: Imported from `react`, this hook is used to perform side effects in functional components.
- **useSelector**: Imported from `react-redux`, this hook is used to extract data from the Redux store state.
- **useDispatch**: Imported from `react-redux`, this hook is used to dispatch actions to the Redux store.
- **Box**: Imported from `@chakra-ui/react`, this component renders a box container for organizing content.
- **Text**: Imported from `@chakra-ui/react`, this component renders text.
- **Button**: Imported from `@chakra-ui/react`, this component renders a clickable button.
- **Flex**: Imported from `@chakra-ui/react`, this component provides a flexible container for aligning its children.
- **Spacer**: Imported from `@chakra-ui/react`, this component adds space between its children.
- **Tooltip**: Imported from `@chakra-ui/react`, this component renders a tooltip for providing additional information on hover.
- **Input**: Imported from `@chakra-ui/react`, this component renders an input field for user input.
- **DeleteIcon, EditIcon, CheckIcon, CloseIcon**: Imported from `@chakra-ui/icons`, these icons are used for various actions.

## Functionality

- **State Management**:
  - `editedTaskText`: Manages the text of the task being edited.
  - `editingTaskId`: Tracks the ID of the task currently being edited.
  - `completedTaskIds`: Tracks the IDs of completed tasks.
- **Effect Hooks**:
  - `useEffect` is used to initialize and persist the `completedTaskIds` in local storage.
- **Event Handlers**:
  - `handleDelete`: Dispatches a `deleteTask` action to remove a task from the list.
  - `handleEdit`: Sets the `editingTaskId` and `editedTaskText` state to enable task editing.
  - `handleUpdate`: Dispatches an `updateTask` action to update the text of a task.
  - `handleCancel`: Cancels the editing mode by resetting the `editingTaskId` and `editedTaskText` state.
  - `handleToggleCompletion`: Toggles the completion status of a task by adding or removing its ID from `completedTaskIds`.
- **Helper Functions**:
  - `isEditing`: Checks if a task is currently being edited.
  - `isCompleted`: Checks if a task is completed based on its ID.
- **Rendering**:
  - Maps through the `tasks` array from the Redux store and renders each task along with appropriate controls (edit, delete).
  - Tasks are displayed with text color and decoration based on completion status.
  - When in edit mode, tasks are rendered with an input field for editing and buttons for updating or canceling.
  - Tooltips provide additional information for the edit and delete buttons.
- **Styling**:
  - The component is styled with a background color of `#8758ff` and a border radius of `md`.
  - Task text color and decoration change dynamically based on completion status.


# Task Slice Explanation

The `taskSlice` file contains a Redux slice that defines actions and reducers for managing tasks in the application state.

## Imports

- **createSlice**: Imported from `@reduxjs/toolkit`, this function is used to create a Redux slice with reducers and initial state.

## Slice Creation

- The `createSlice` function is used to create a slice named "tasks".
- Initial state is set to an empty array.

## Reducers

1. **addTask**: Adds a new task to the state array.
   - Action Payload: Contains the task object to be added.
   - Logic: Pushes the new task object into the state array.

2. **deleteTask**: Deletes a task from the state array.
   - Action Payload: Contains the ID of the task to be deleted.
   - Logic: Filters out the task with the specified ID from the state array.

3. **updateTask**: Updates the text of an existing task.
   - Action Payload: Contains the ID of the task to be updated and the new text.
   - Logic: Finds the index of the task with the specified ID in the state array and updates its text property.

## Exported Actions and Reducer

- The actions (`addTask`, `deleteTask`, `updateTask`) and the reducer are exported from the file.
- These exported functions can be used directly in Redux action creators and reducers.

## Usage

- These actions can be dispatched from any component or Redux action creator to update the tasks in the Redux store state.
- The reducer defined in this file is combined with other reducers using Redux's `combineReducers` function when creating the Redux store.

## Note

- This slice follows the "slice of state" pattern where each slice of the Redux store state corresponds to a specific feature or domain in the application.
- It uses Immer under the hood to allow writing simpler immutable update logic.

# Store Configuration Explanation

The `store.js` file contains the configuration for the Redux store, including the root reducer and any middleware.

## Imports

- **configureStore**: Imported from `@reduxjs/toolkit`, this function is used to create the Redux store.
- **combineReducers**: Imported from `redux`, this function is used to combine multiple reducers into a single root reducer.
- **taskSlice**: Imported from the relative path `"../taskSlice"`, this is the reducer slice for managing tasks.

## Root Reducer

- The `rootReducer` combines all reducer slices into a single root reducer using the `combineReducers` function.
- In this case, only one reducer slice (`taskSlice`) is included.

## Store Configuration

- The `configureStore` function is called with an object argument.
  - The `reducer` key is set to the `rootReducer`, providing the root reducer to the store configuration.
  - Additional configuration options such as middleware can be provided here if needed.

## Export

- The configured Redux store is exported from the file.
- This store is now ready to be used in the application to manage state.

## Usage

- This store configuration can be imported into the application's main entry point (e.g., `index.js`) and provided to the `Provider` component from `react-redux`.
- Once provided, the store will be accessible to all components in the application, allowing them to interact with the Redux state.

## Note

- The usage of `configureStore` from `@reduxjs/toolkit` simplifies the store setup by including several pieces of Redux logic such as Redux DevTools Extension integration and default middleware setup.
- Combining reducers using `combineReducers` allows for a modular approach to managing the Redux store state, with each reducer slice responsible for a specific part of the state.
