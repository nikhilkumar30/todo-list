import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTask } from "./taskSlice";
import { Input, Button, Flex } from "@chakra-ui/react";

function TaskInput() {
  const [task, setTask] = useState("");
  const dispatch = useDispatch();

  const handleChange = (e) => {
    setTask(e.target.value);
  };

  const handleSubmit = () => {
    if (task.trim() !== "") {
      dispatch(addTask({ id: Date.now(), text: task }));
      setTask("");
    }
  };

  return (
    <Flex align="center" flexDirection={{ base: "column", md: "row" }}>
      <Input
        type="text"
        value={task}
        onChange={handleChange}
        placeholder="What is the task today?"
        sx={{ "::placeholder": { color: "#ffffff4d" } }}
        mr={2}
        textColor="white"
        bg="#8758ff"
      />
      <Button onClick={handleSubmit} ml={2} bg="#8758ff">
        Add Task
      </Button>
    </Flex>
  );
}

export default TaskInput;
