import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import taskSlice from "../taskSlice";

const rootReducer = combineReducers({
  tasks: taskSlice,
});

const store = configureStore({
  reducer: rootReducer,
});

export default store;
